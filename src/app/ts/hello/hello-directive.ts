import IDirectiveFactory = angular.IDirectiveFactory;
import {Directive,Inject} from '../ng-decorators';
import IDirective = angular.IDirective;
import ITimeoutService = angular.ITimeoutService;
import IAugmentedJQuery = angular.IAugmentedJQuery;
import {BackendService} from '../service/backend-service';
import IScope = angular.IScope;
import {Widget} from "../widgets/widgets.manager";
import {WidgetState} from "../widgets/widgets.manager";


@Directive({
    selector: 'hello-widget'
})
class HelloDirective {

    static instance:HelloDirective;
    public restrict = 'E';
    public bindToController = true;
    public controllerAs = 'hello';
    public controller = HelloController;
    public template = '<is-widget widget-controller="hello"><H1 ng-class="hello.stateClass">{{hello.theText}}</H1></is-widget>';
    public scope = {
        theText: "=",
    };

    constructor() {}

    static directiveFactory() {
        HelloDirective.instance = new HelloDirective();
        return HelloDirective.instance;
    }

}

@Inject("$element",'$scope')
class HelloController implements Widget{

    state:WidgetState = WidgetState.LOADING;

    constructor(public element:IAugmentedJQuery, private scope:IScope) {
        this.ready();
    }

    ready():Promise<void> {
        return BackendService.callBackend()
            .then((result) => {this.state = WidgetState.READY})
            .catch((result) => {this.state = WidgetState.ERROR});
    }

}