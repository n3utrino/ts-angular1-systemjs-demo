import './hello-directive';
import {RouteConfig} from "../ng-decorators";


// config for http://127.0.0.1:8888/#/hello
@RouteConfig('hello', {
    url: '/hello',
    abstract: 'true',
    template: '<ui-view/>'
})
class HelloRoute {

    public text:string = "Hello Worlds";

    constructor() {
        console.log("Hello Route Controller");
    }
}

@RouteConfig('hello.uebersicht', {
    url: '',
    template: '<hello-widget the-text="vm.text"></hello-widget>'
})
class HelloUebersicht {

    public text:string = "Hello World";

    constructor() {
        console.log("Hello Route Controller");
    }
}

// config for http://127.0.0.1:8888/#/hello/banana
@RouteConfig('hello.banana', {
    url: '/banana',
    template: '<hello-widget the-text="bananaController.text"></hello-widget>',
    controllerAs: 'bananaController'
})
class HelloBanana {

    public text:string = "Hello Bananas";

    constructor() {
        console.log("Hello Banana Route Controller");
    }
}