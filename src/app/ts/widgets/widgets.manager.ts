import IAugmentedJQuery = angular.IAugmentedJQuery;
import {Injector} from "../ng-decorators";
import IRootScopeService = angular.IRootScopeService;
import IScope = angular.IScope;


export enum WidgetState {READY, LOADING, ERROR}

export interface Widget {
    state:WidgetState;
}

class WidgetManagerImpl {

    private widgets:{[id:string] : Widget} = {};


    public add(widget:Widget):void {
        this.widgets[widget.id] = widget;
    }

}

export const WidgetManager = new WidgetManagerImpl();