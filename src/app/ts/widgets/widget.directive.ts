import {Directive} from "../ng-decorators";
import {Inject} from "../ng-decorators";
import IDirective = angular.IDirective;
import IScope = angular.IScope;
import IAugmentedJQuery = angular.IAugmentedJQuery;
import IAttributes = angular.IAttributes;
import ITranscludeFunction = angular.ITranscludeFunction;
import {Widget} from "./widgets.manager";
import {WidgetState} from "./widgets.manager";

@Directive({
    selector: 'is-widget'
})
class WidgetDirective implements IDirective {

    static instance:WidgetDirective;
    public restrict = 'E';
    public controllerAs = 'widget';
    public controller = WidgetDirectiveController;
    public transclude = true;
    public bindToController = {
        widgetController: "=",
    };
    public template = '<div class="{{widget.getClass()}}"><div ng-show="widget.isLoading()">Loading</div>' +
        '<ng-transclude></ng-transclude>' +
        '</div>';


    constructor() {
    }

    static directiveFactory() {
        WidgetDirective.instance = new WidgetDirective();
        return WidgetDirective.instance;
    }

}

@Inject('$element')
class WidgetDirectiveController {

    public widgetController:Widget;
    public stateClass = 'widget-loading';

    constructor(private $element:IAugmentedJQuery) {
        console.log("Widget Controller");
    }

    public isLoading() {
        return this.widgetController.state === WidgetState.LOADING;
    }

    public getClass():string {

        switch (this.widgetController.state) {
            case WidgetState.LOADING:
                return 'widget-loading';
            case WidgetState.ERROR:
                return 'widget-error';
            case WidgetState.READY:
                return 'widget-ready';
        }

    }

}