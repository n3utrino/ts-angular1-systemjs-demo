import {Component} from 'angular2/core';
import {UpgradeAdapter} from 'angular2/upgrade';
import {adapter} from '../ng-decorators';
import app from '../ng-decorators';
import {BackendService} from "../service/backend-service";
import IDirectiveFactory = angular.IDirectiveFactory;

@Component({
    selector: 'hello-2',
    template: `
    <h1>Hello 2</h1>
  `
})
class Hello2 {

    ready():Promise<void> {
        return BackendService.callBackend();
    }

}

app.directive("hello2", <IDirectiveFactory>adapter.downgradeNg2Component(Hello2));