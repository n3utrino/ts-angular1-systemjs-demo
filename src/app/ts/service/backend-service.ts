import ITimeoutService = angular.ITimeoutService;
import IRootScopeService = angular.IRootScopeService;

class Backend {

    public callBackend():Promise<any> {

        return new Promise<void>((resolve, reject)=> {
            setTimeout(this.backendFunction.bind(this, resolve, reject), Math.random() * 5000);
        });

    }

    private backendFunction(resolve:Function, reject:Function) {
        let doReject = Math.random() > 0.5;
        if (doReject) {
            reject();
        } else {
            resolve();
        }

    }
}

export const BackendService = new Backend();