import * as angular from 'angular';

import {UpgradeAdapter} from 'angular2/upgrade';
import IDirective = angular.IDirective;
import IDirectiveFactory = angular.IDirectiveFactory;
import IStateProvider = angular.ui.IStateProvider;

export const adapter = new UpgradeAdapter();
const app = angular.module('demoApp', [
    //Add 3rd party angular dependencies
    'ui.router'
]);


export function Directive(options:any) {
    return function decorator(target:any) {
        const directiveName = dashCaseToCamelCase(options.selector);
        app.directive(directiveName, target.directiveFactory);
    };
}

export function Inject(...dependencies:string[]):ClassDecorator {
    return function decorator(target:any) {
        target.$inject = dependencies;
    };
}

export function Injector(injectable:string):PropertyDecorator{
    return function decorator(target:any,property:string) {
        target[property] = angular.injector([app.name]).get(injectable);
    };
}

export function RouteConfig(stateName:string, options:any) {
    return function decorator(target:any) {
        app.config(['$stateProvider', ($stateProvider:IStateProvider) => {
            $stateProvider.state(stateName, _.assign({
                controller: target,
                controllerAs: 'vm'
            }, options));
        }]);
        app.controller(target.name, target);
    };
}

function dashCaseToCamelCase(string:string) {
    return string.replace(/-([a-z])/ig, function (all, letter) {
        return letter.toUpperCase();
    });
}


export default app;