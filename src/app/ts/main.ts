/// <reference path="../../../typings/browser.d.ts" />
import * as angular from 'angular';
import 'angular2/core';

//Imports for 3rd party modules (this loads the js)
import 'angular-ui-router';
import 'lodash';

import './hello/hello-module'
import './widgets/widget.directive.ts'
import './helloNg2/helloComponent'

import {adapter} from './ng-decorators';
import mainModule from './ng-decorators';


angular.element(document).ready(function() {

    console.log("bootstrapping");
    adapter.bootstrap(document.body, [mainModule.name], {
        strictDi: true
    });
});



