import {TypeAction,TypedActionsService} from './typedActions';

describe("ActionSevice", () =>{

    it('should register actions', () => {
        let service = new TypedActionsService();
            service.registerAction(new TypeAction());
        expect(service.findActions("").length).toBe(1);
    });


});

describe("TypeAction", () =>{

    it('should execute the Function', () => {
        let typeAction = new TypeAction();
        typeAction.action = "erfassen";
        typeAction.object = "Zahlung";
        typeAction.subject = "Kunde";

        let shouldBeTrue = false;

        typeAction.what = (who:boolean)=>{shouldBeTrue = who};
        typeAction.who = true;

        typeAction.execute();

        expect(shouldBeTrue).toBeTruthy();

    });


});
