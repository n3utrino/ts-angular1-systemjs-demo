export class TypeAction {

    public object:string;       //Zahlung
    public action:string;       //erfassen
    public subject:string;      //Kunde / Konto

    public alias:string[];

    public what:Function;
    public who:any;

    public execute(){
        this.what(this.who);
    }

}

export class TypedActionsService{

    private registeredActions:TypeAction[] = [];

    public findActions(name:string):TypeAction[]{
        return this.registeredActions;
    }

    public registerAction(action:TypeAction){
        this.registeredActions.push(action);
    }


}

class NameFuzzer{
    //Creates aliases
}