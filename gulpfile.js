var gulp = require('gulp');
var sass = require('gulp-sass');
var gls = require('gulp-live-server');

var sassFiles = ['src/app/assets/styles/**/*.scss',
    'src/app/ts/**/*.scss'];

gulp.task('styles', function() {
    gulp.src(sassFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/app/assets/styles/'));
});

//Watch task
gulp.task('default',function() {
    var server = gls.static('src/app', 8888);
    server.start();

    gulp.watch(sassFiles,['styles']);
    gulp.watch([
        'src/app/index.html',
        'src/app/ts/**/*.html',
        'src/app/ts/**/*.ts',
        'src/app/assets/*.css'], function (file) {
        server.notify.apply(server, [file]);
    });
});