How To Run
==========

* install node.js
*   ´npm install jspm typescript typings -g´
*   ´npm install´
*   ´gulp´

Used Technologies
=================

* jspm for package management
* gulp for build
* sass for styles
* system.js for module loading
* typescript
* angular 1.4.10 for application
* angular2 with angular/upgrade for mixed mode
* typings for ambient type definition management (definetly-typed repo)