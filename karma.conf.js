module.exports = function (config) {
    config.set({

        frameworks: ['jspm', 'jasmine'],

        files: [
            'node_modules/karma-babel-preprocessor/node_modules/babel-core/browser-polyfill.js',
            'node_modules/jasmine-async-sugar/jasmine-async-sugar.js',
            'node_modules/zone.js/dist/zone.js',
            'build/Reflect.js'
        ],

        jspm: {
            config: 'build/jspm.config.js',
            loadFiles: ['zone.js','build/app/js/main.js', 'build/app/**/*.spec.js'],
            serveFiles: ['test/helpers/**/*.js','build/app/**/*.+(js|html|css|json)']
        },

        proxies: {
            '/test/': '/base/test/',
            '/build/': '/base/build/',
            '/jspm_packages/': '/base/src/app/jspm_packages/'
        }

    });
};